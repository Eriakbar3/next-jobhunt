import React from 'react';

import { Layout } from 'antd';
import './index.css';
import Image from 'next/image';
import { menuList, menuSetting } from '@/utils/menuConfig';
import Link from 'next/link';
const { Sider } = Layout;

export default function SideBar() {
  return (
    <>
      <Sider className="sidebar" width={272}>
        <div className="sidebar-logo">
          <Image src="/assets/logo.png" width={160} height={36} />
        </div>
        <div className="sidebar-menu">
          <ul className="main-menu">
            {menuList.map((data, index) => {
              return <li><Link href={data.route}>{data.label}</Link></li>;
            })}
          </ul>
          <div className="setting-menu">
            <label>Settings</label>
            <ul className="main-menu">
              {menuSetting.map((data, index) => {
                return <li>{data.label}</li>;
              })}
            </ul>
          </div>
        </div>
        <div className="sidebar-footer"></div>
      </Sider>
    </>
  );
}
