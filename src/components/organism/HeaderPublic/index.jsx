import React from 'react';
import './index.css';
import { Button } from 'antd';
export default function HeaderPublic() {
  return (
    <div className="header">
      <div className="header-left">
        <div className="logo">
          <img src="/assets/image/logo.png" />
        </div>
        <div>
          <ul className="menu-list">
            <li className="list-active">Find Jobs</li>
            <li>Browse Companies</li>
          </ul>
        </div>
      </div>
      <div className="header-right">
        <Button className="btn-login" size="middle">
          Login
        </Button>
        <div style={{ width: 1, background: '#D6DDEB' }}></div>
        <Button type="primary">Register</Button>
      </div>
    </div>
  );
}
