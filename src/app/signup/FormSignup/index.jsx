'use client';
import React, { useState } from 'react';
import { Button, Form, Input, Typography, Radio, message } from 'antd';
const { Title, Text } = Typography;
import './index.css';
import { useDispatch, useSelector } from 'react-redux';
import { actionRegister } from '@/redux/user/action';
import { useRouter } from 'next/navigation';
export default function FormSignup() {
  const [mode, setMode] = useState('top');
  const dispatch = useDispatch();
  const { loading } = useSelector((state) => state.user);
  const router = useRouter();
  const handleModeChange = (e) => {
    setMode(e.target.value);
  };
  const register = async (value) => {
    if (value.password !== value.confirm_password) {
      message.error("confirm password doesn't match");
      return;
    }
    const response = await dispatch(actionRegister(value));
    if (response.success) {
      message.success(response.message);
      router.push('login');
    } else {
      message.error(response.message);
    }
  };
  return (
    <div className="form-wrapper">
      <Radio.Group onChange={handleModeChange} value={mode} style={{ marginBottom: 8 }}>
        <Radio.Button value="top">Job Seeker</Radio.Button>
        <Radio.Button value="left">Company</Radio.Button>
      </Radio.Group>
      <Title level={3}>Get more opportunities</Title>
      <Button className="btn-google">Sign Up with Google</Button>
      <div className="text-line">
        <Text style={{ backgroundColor: '#FFFFFF', padding: '0 15px' }}>Or sign up with email</Text>
        <div className="line"></div>
      </div>
      <Form layout="vertical" onFinish={register}>
        <Form.Item label="Full Name" name="name">
          <Input />
        </Form.Item>
        <Form.Item label="Email" name="email">
          <Input />
        </Form.Item>
        <Form.Item label="Password" name="password">
          <Input.Password />
        </Form.Item>
        <Form.Item label="Confirm Password" name="confirm_password">
          <Input.Password />
        </Form.Item>
        <Button
          type="primary"
          style={{ width: '100%', fontWeight: 'bold' }}
          htmlType="submit"
          loading={loading}>
          Continue
        </Button>
      </Form>
      <p>
        Already have an account? <a>Login</a>
      </p>
      <p>
        By clicking 'Continue', you acknowledge that you have read and accept the{' '}
        <a>Terms of Service </a>
        and <a>Privacy Policy</a>.
      </p>
    </div>
  );
}
