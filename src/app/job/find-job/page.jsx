import React from 'react';
import HeaderTemplate from '@/components/organism/HeaderPublic';
import SearchContent from './SearchContent';
import MainContent from './MainContent';
import FooterTemplate from '@/components/organism/footer';
export default function FindJob() {
  return (
    <div>
      <HeaderTemplate />
      <SearchContent />
      <MainContent />
      <FooterTemplate />
    </div>
  );
}
