import React, { useState } from 'react';
import { Button, Form, Input, Typography, Radio, Checkbox, message } from 'antd';
const { Title, Text } = Typography;
import './index.css';
import { useDispatch } from 'react-redux';
import { actionSignin } from '@/redux/user/action';
import { useRouter } from 'next/navigation';
export default function FormSignup() {
  const [mode, setMode] = useState('top');
  const dispatch = useDispatch();
  const router = useRouter();
  const handleModeChange = (e) => {
    setMode(e.target.value);
  };
  const login = async (value) => {
    const response = await dispatch(actionSignin(value));
    if (response.success) {
      message.success(response.message);
      router.push('/dashboard');
    } else {
      message.error(response.message);
    }
  };
  return (
    <div className="form-wrapper">
      <Radio.Group onChange={handleModeChange} value={mode} style={{ marginBottom: 8 }}>
        <Radio.Button value="top">Job Seeker</Radio.Button>
        <Radio.Button value="left">Company</Radio.Button>
      </Radio.Group>
      <Title level={3}>Welcome Back, Dude</Title>
      <Button className="btn-google">Login with Google</Button>
      <div className="text-line">
        <Text style={{ backgroundColor: '#FFFFFF', padding: '0 15px' }}>Or login with email</Text>
        <div className="line"></div>
      </div>
      <Form layout="vertical" onFinish={login}>
        <Form.Item label="Email" name={'email'}>
          <Input />
        </Form.Item>
        <Form.Item label="Password" name={'password'}>
          <Input.Password />
        </Form.Item>
        <Form.Item name="remember" valuePropName="checked">
          <Checkbox>Remember me</Checkbox>
        </Form.Item>
        <Button type="primary" style={{ width: '100%', fontWeight: 'bold' }} htmlType="submit">
          Continue
        </Button>
      </Form>
      <p>
        Already have an account? <a>Sign up</a>
      </p>
    </div>
  );
}
