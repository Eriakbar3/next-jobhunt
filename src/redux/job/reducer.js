const initialState = {
    loading:false,
    job:[],
    jobType:[],
    jobCategory:[],
    jobLevel:[],
    jobSalary:[],
}

export default function reducer(state=initialState,action){
    switch (action.type) {
        case 'job_loading':
            return{
                ...state,
                loading:action.payload.loading
            }
        case 'job_success':
            return{
                ...state,
                job:action.payload.job,
                loading:action.payload.loading
            }
        case 'job_type_success':
            return{
                ...state,
                jobType:action.payload.jobType,
                jobCategory:action.payload.jobCategory,
                jobLevel:action.payload.jobLevel,
                jobSalary:action.payload.jobSalary,
                loading:action.payload.loading
            }
    
        default:
            return state;
    }
}